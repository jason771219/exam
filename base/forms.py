from .models import Tutorial,Todos,Expense
from django.conf import settings
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm
import re

# Test Form
class ExpenseModelForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = ('name', 'price')# or fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'price': forms.NumberInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': '花費項目',
            'price': '金額'
        }