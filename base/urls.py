

from django.urls import include,path
from . import views

base64_pattern = r'(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$'

urlpatterns = [
    
    path('', views.index, name='Index'),
    path('update/<str:pk>', views.update, name='Update'),
    path('delete/<str:pk>', views.delete, name='Delete'),


    path('v1/api/todos/<int:st>/', views.TodosView.as_view()),  # filter status
    path('v1/api/user/<int:user_id>/', views.UsersView.as_view()),  # user status

]