from django.db import models

# Create your models here.

# Test Model
class Tutorial(models.Model):
    title = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200,blank=False, default='')
    published = models.BooleanField(default=False)

    def as_json(self):
        return dict(
            id=self.id, 
            title=self.title,
            description=self.description, 
            published=self.published)    


# Exam Model
class Todos(models.Model):
    title 		= models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200,blank=False, default='')
    status		= models.IntegerField(default=0, null=True, verbose_name="狀態")

    def as_json(self):
        return dict(
            id=self.id, 
            title=self.title,
            description=self.description, 
            status=self.status)    
    class Meta:
    	verbose_name = 'Todos'
    	verbose_name_plural = 'Todos'
    def __str__(self):
    	return self.title    

# Test Model
class Expense(models.Model):
    name = models.CharField(max_length=255)  #花費項目
    price = models.IntegerField() #金額