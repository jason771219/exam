from django.contrib import admin

# Register your models here.
from .models import Tutorial,Todos,Expense

class TodosAdmin(admin.ModelAdmin):    
    list_display = ('title', 'description', 'status')        
    search_fields = ['title']
    ordering = ('-title',)
    list_filter = ("status", )
    
admin.site.register(Todos, TodosAdmin)



class ExpenseAdmin(admin.ModelAdmin):    
    list_display = ('name', 'price')        
    search_fields = ['name']
    ordering = ('-name',)


admin.site.register(Expense, ExpenseAdmin)