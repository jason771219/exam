from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sites.shortcuts import get_current_site
from django.core.files import File
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
from django.db.models import Sum
from django.db.models import Q
from django.db.models.functions import TruncMonth, TruncDate
from django.http import QueryDict
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings
from django.http.response import JsonResponse
from django.contrib.auth.models import User, Group

from rest_framework import status
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework import routers, serializers, viewsets
from rest_framework.decorators import action


from .models import Tutorial,Todos,Expense
from .serializer import TutorialSerializer,Todoserializer,Userserializer,GroupSerializer,ExpenseSerializer
from .forms import ExpenseModelForm


import os
from os.path import splitext
import json
import sys
import traceback
import datetime
import calendar
import zipfile
import logging
import time
import threading


# RestFramework API Example
class TodosView(APIView):
	# Need write login api Get Token.
	# authentication_classes = (TokenAuthentication,)
	# permission_classes = (IsAuthenticated,)

	def get(self, request, st, format=None):
		is_check_status = False;
		if st == settings.STORE_STATUS_COMPLETE:
			is_check_status = True;

		if st == settings.STORE_STATUS_IN_PROCESS:
			is_check_status = True;

		if st == settings.STORE_STATUS_HATE_IT:
			is_check_status = True;

		if is_check_status:
		    try: 
		        todos = Todos.objects.filter(status=st) 
		    except Todos.DoesNotExist: 
		        return JsonResponse({'message': 'The Todos does not exist'}, status=status.HTTP_404_NOT_FOUND) 

		    todos_serializer = Todoserializer(todos, many=True)
		    return JsonResponse(todos_serializer.data,safe=False) 		        
		else:
	 		return JsonResponse({'message': 'The Todos status does not exist'}, status=status.HTTP_404_NOT_FOUND) 



#Rest Api  GET , POST, DELETE 範例
class TutorialListView(APIView):
	#限制用正確的token才有使用權限
	# authentication_classes = (TokenAuthentication,)
	# permission_classes = (IsAuthenticated,)

	def get(self, request, format=None):
		tutorials = Tutorial.objects.all()
		tutorials_serializer = TutorialSerializer(tutorials, many=True)
		return JsonResponse(tutorials_serializer.data, safe=False)

	def delete(self, request, format=None):
		count = Tutorial.objects.all().delete()
		return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)

	def post(self, request, format=None):        
		request_data = request.data
		try:
			title = request_data['title']
			description = request_data['description']
			tutorial = Tutorial();
			tutorial.title = title;
			tutorial.description = description;
			tutorial.save();

			dic = tutorial.as_json()
			r = {'status':0, 'message':'Create OK','tutorial':dic}
			return JsonResponse(r, status=status.HTTP_201_CREATED)

			
		except KeyError as error:
			# Output expected KeyErrors.
			traceback.print_exc()
			r = {'status':-3, 'message':'KeyError'}
			return JsonResponse(r, status=status.HTTP_404_NOT_FOUND)
		except:
			r = {'status':0, 'message':'UnknownError'}
			return JsonResponse(r, status=status.HTTP_400_BAD_REQUEST) 	


class TutorialDetailView(APIView):
	#限制用正確的token才有使用權限
	# authentication_classes = (TokenAuthentication,)
	# permission_classes = (IsAuthenticated,)

	def get(self, request, pk, format=None):
	    try: 
	        tutorial = Tutorial.objects.get(pk=pk) 
	    except Tutorial.DoesNotExist: 
	        return JsonResponse({'message': 'The tutorial does not exist'}, status=status.HTTP_404_NOT_FOUND) 
	 
	    tutorial_serializer = TutorialSerializer(tutorial) 
	    return JsonResponse(tutorial_serializer.data) 

	def put(self, request, pk, format=None):
		tutorial_data = JSONParser().parse(request) 
		tutorial_serializer = TutorialSerializer(tutorial, data=tutorial_data) 
		if tutorial_serializer.is_valid(): 
			tutorial_serializer.save() 
			return JsonResponse(tutorial_serializer.data) 
		return JsonResponse(tutorial_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 

	def delete(self, pk , request, format=None):        
		# raw json use

		# tutorial_data = JSONParser().parse(request)
		# print("tutorial_data:",tutorial_data)
		# tutorial_serializer = TutorialSerializer(data=tutorial_data)
		# if tutorial_serializer.is_valid():
		# 	tutorial_serializer.save()
		# 	return JsonResponse(tutorial_serializer.data, status=status.HTTP_201_CREATED) 
		# return JsonResponse(tutorial_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

		#  urlecoded  use
		request_data = request.data
		try:
			title = request_data['title']
			description = request_data['description']
			tutorial = Tutorial();
			tutorial.title = title;
			tutorial.description = description;
			tutorial.save();

			dic = tutorial.as_json()
			r = {'status':0, 'message':'Create OK','tutorial':dic}
			return JsonResponse(r, status=status.HTTP_201_CREATED)

			
		except KeyError as error:
			# Output expected KeyErrors.
			traceback.print_exc()
			r = {'status':-3, 'message':'KeyError'}
			return JsonResponse(r, status=status.HTTP_404_NOT_FOUND)
		except:
			r = {'status':0, 'message':'UnknownError'}
			return JsonResponse(r, status=status.HTTP_400_BAD_REQUEST) 	



class UsersView(APIView):
	# Need write login api Get Token.
	# authentication_classes = (TokenAuthentication,)
	# permission_classes = (IsAuthenticated,)

	def get(self, request, user_id, format=None):
	    try: 
	        user = User.objects.get(id=user_id) 
	    except User.DoesNotExist: 
	        return JsonResponse({'message': 'The User does not exist'}, status=status.HTTP_404_NOT_FOUND) 
	 
	    serializer = Userserializer(user) 
	    return JsonResponse(serializer.data) 


#  Render Html....
def index(request):
    expenses = Expense.objects.all()  # 查詢所有資料
    form = ExpenseModelForm()
    if request.method == "POST":
        form = ExpenseModelForm(request.POST)
        print("index...")
        if form.is_valid():
            form.save()
        return redirect("/base")

    
    serializer = ExpenseSerializer(expenses, many=True)
    expense_data = serializer.data;
    print("expense_data:",expense_data);
    context = {
        'expenses': expenses,
        'form': form,
        'expense_data':json.dumps(expense_data)
    }
    return render(request, 'expenses/index.html', context)


def update(request, pk):
    expense = Expense.objects.get(id=pk)
    form = ExpenseModelForm(instance=expense)
    if request.method == 'POST':
        form = ExpenseModelForm(request.POST, instance=expense)
        if form.is_valid():
            form.save()
        return redirect('/base')
    context = {
        'form': form
    }
    return render(request, 'expenses/update.html', context)



def delete(request, pk):
    expense = Expense.objects.get(id=pk)
    if request.method == "POST":
        expense.delete()
        return redirect('/base')
    context = {
        'expense': expense
    }
    return render(request, 'expenses/delete.html', context)


# RestFramework ModeViewSet ...
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = Userserializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]   


# Normal write...
# class TodosViewSet(viewsets.ModelViewSet):
#     queryset = Todos.objects.all()
#     serializer_class = Todoserializer


# Custom write...
class TodosViewSet(viewsets.ModelViewSet):
    queryset = Todos.objects.all()
    serializer_class = Todoserializer
    # permission_classes = [permissions.IsAuthenticated]
    # parser_classes = (JSONParser,) #Allow contenttype application/json,default contenttype application / x-www-form-urlencode 

    # Override get_permissions
    def get_permissions(self):
        if self.action in ('create',):
            self.permission_classes = [IsAuthenticated]
        return [permission() for permission in self.permission_classes]

    # Test get not use Auth 
    # Override [GET] todos/
    def list(self, request, **kwargs):
        todos = Todos.objects.all()
        serializer = Todoserializer(todos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Override [POST] todos/
    def create(self, request, **kwargs):
        title = request.data.get('title')
        description = request.data.get('description')
        st = request.data.get('status')
        todos = Todos.objects.create(title=title,description=description,status=st)
        serializer = Todoserializer(todos)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    # Override [UPDATE] todos/id
    def update(self, request, *args, **kwargs):
    	print("args:",args);
    	print("kwargs:",kwargs['pk']);
    	# kwargs: {'pk': '2'}
    	return super().update(request,args,kwargs)

    # Override [DELETE] todos/id
    def destroy(self, request, *args, **kwargs):
    	print("args:",args);
    	print("kwargs:",kwargs['pk']);
    	return super().destroy(request,args,kwargs)