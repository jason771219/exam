#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers


from .models import Tutorial,Todos,Expense
from django.contrib.auth.models import User,Group

class TutorialSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tutorial
        fields = '__all__'

class Todoserializer(serializers.ModelSerializer):

    class Meta:
        model = Todos
        fields = '__all__'


class Userserializer(serializers.ModelSerializer):

    class Meta:
        model = User
        # fields = '__all__'
        fields = ['url', 'username', 'email', 'is_staff']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class ExpenseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expense
        fields = '__all__'

        