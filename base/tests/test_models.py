from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from base.models import Tutorial,Todos
from django.conf import settings

class TodosTestModelCase(TestCase):
    def setUp(self):
        Todos.objects.create(title="TitleTest1", description="TitleTest1",status=settings.STORE_STATUS_IN_PROCESS);
        Todos.objects.create(title="TitleTest2", description="TitleTest2",status=settings.STORE_STATUS_IN_PROCESS);

    def test_todos(self):
        todos1 = Todos.objects.get(title="TitleTest1")
        todos2 = Todos.objects.get(title="TitleTest2")
        self.assertEqual(todos1.description,"TitleTest1")#判斷 兩個值是否等於
        self.assertEqual(todos2.description,"TitleTest2")

    def test_print(self):
    	print("test print---")