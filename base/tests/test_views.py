from django.test import TestCase
from ..models import Tutorial,Todos
from django.conf import settings
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from ..views import TodosViewSet
from django.contrib.auth.models import User, Group

class TodosTestViewCase(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()

    def test_view(self):
        user = User.objects.create(username='testuser')
        user.set_password('a0932958583')
        user.save()

        login_result = self.client.login(username='testuser', password='a0932958583')
        # response = self.client.get(reverse('todos'))
        print("login_result:",login_result);

        create_todo_response = self.client.post('/todos/', {'title': 'john', 'description': 'smith','status':0})
        print("create todo response:",create_todo_response.json());
        print("response httpstatus code :",create_todo_response.status_code);        

        todo_list = self.client.get('/todos/')
        print("todo_list jason:",todo_list.json());        


        