from django.test import TestCase
from ..models import Tutorial,Todos,Expense
from django.conf import settings
from http import HTTPStatus
from ..forms import ExpenseModelForm

class ExpenseModelFormTests(TestCase):

    def test_form_submit(self):
        form_data = {"name": "Jason","price":"9999"}
        form = ExpenseModelForm(data=form_data)
        self.assertTrue(form.is_valid())
        